My version of wg-dashboard install script for Ubuntu 20.04. Forked from here https://github.com/wg-dashboard/wg-dashboard

Uses cloudflared instead of CoreDNS, **Doesn't work with any OS except for Ubuntu 20.04** :) 

Also by default uses https://security.cloudflare-dns.com/dns-query/ to block malware. 

You can change it to https://1.1.1.1/dns-query if you don't care.
